public class Calculator {
    private int _a;
    private int _b;

    public Calculator(){}

       public Calculator(int a, int b){
            _a=a;
            _b=b;
        }


    public double sum(double a, double b) {
        double result = a + b;
        return result;
    }

    public int subtract(int a, int b) {
        int result = a - b;
        return result;
    }

    public long multiplication(long a, long b) {
        long result = a * b;
        return result;
    }

    public float devide(float a, float b) {
        float result = a/b;
        return result;
    }

    public int sum(){
        return _a+_b;
    }

    public int subtract(){
        return _a-_b;
    }

    public int multiplication(){
        return _a*_b;
    }

}