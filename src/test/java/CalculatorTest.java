import org.junit.Assert;
import org.junit.Test;


public class CalculatorTest {

    @Test
    public void testSum1(){
        Calculator calc=new Calculator();
        double actual= calc.sum(3,5.5);
        Assert.assertEquals(8.5,actual,0.01);
    }

    @Test
    public void testSubtract(){
        Calculator calc=new Calculator();
        int actual= calc.subtract(8,4);
        Assert.assertEquals(4,actual);
    }

    @Test
    public void testMultiplication(){
        Calculator calc=new Calculator();
        long actual= calc.multiplication(50,50);
        Assert.assertEquals(2500,actual);
    }

    @Test
    public void testDevide(){
        Calculator calc=new Calculator();
        float actual= calc.devide(100.0F,20.0F);
        Assert.assertEquals(5F,actual,0.0001);
    }

    @Test
    public void testSum2(){
            Calculator calculator=new Calculator(15,5);
            int actual= calculator.sum();
            Assert.assertEquals(20, actual);
    }

    @Test
    public void testSubtract2(){
        Calculator calculator= new Calculator(15,5);
        Assert.assertEquals(10,calculator.subtract());
    }

    @Test
    public void testMultiplication2(){
        Calculator calculator=new Calculator(2,3);
        Assert.assertEquals(6,calculator.multiplication());
    }

}
